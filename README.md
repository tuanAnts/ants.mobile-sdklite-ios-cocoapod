

## Requirements
```
FBAudienceNetwork version  4.13.1
```

## Installation

CocoaPods :

ANTSSDKLite is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ANTSSDK"
```

Manual :

Add the ANTSSDKLite project to your project
Download and unzip the last version of the framework from the download page
Right-click on the project navigator and select "Add Files to "Your Project":
In the dialog, select ANTSSDK.framework:
Check the "Copy items into destination group's folder (if needed)" checkbox
Add dependencies

In you application project app’s target settings, find the "Build Phases" section and open the "Link Binary With Libraries" block:
Click the "+" button again and select the "ANTSSDK.framework", this is needed by the progressive download feature:


Set up the ANTS AdX SDK Lite v1.0.0
Download zip  file from : [ANTSADXLite](http://google.com.vn)

##How to Use

Just  #import  the header file in  
```objective-c
#import <AdsWebSDK/ANTSSDK.h>
```

In AppDelegate.m file , call init ANTSSDK function in didFinishLaunchingWithOptions 

```objective-c

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
// Override point for customization after application launch.
[ANTSManager share] ;
return YES;
}
```
## Load ANTS AD

Adding ANTS Ads 320 x 50 to your IOS app

```ruby
@interface ViewController ()< CustomAdDelegate> 
@property (strong, nonamatic)  ANTSAd * ad ;
...
- (void)viewDidLoad {
[super viewDidLoad];
_ad = [[ANTSAd alloc] initWithType:kADANTS 
AndSize:ANTSAdSizeHeight50Rectangle  delegate:self];

[_ad loadADWithURI:@"URI"];
[self.view addSubview:[_ad getADView]];
}
```

Load Content ANTS Ads size 320 x 250

```ruby
@interface ViewController ()< CustomAdDelegate> 
@property (strong, nonamatic)  ANTSAd * ad ;
...
- (void)viewDidLoad {
[super viewDidLoad];
_ad = [[ANTSAd alloc] initWithType:kADANTS 
AndSize:ANTSAdSizeHeight250Rectangle  delegate:self];

[_ad loadADWithURI:@"URI"];
[self.view addSubview:[_ad getADView]];
}
```
Adding ANTS Ads 300 x 600 to your ios app


```ruby
@interface ViewController ()< CustomAdDelegate> 
@property (strong, nonamatic)  ANTSAd * ad ;
...
- (void)viewDidLoad {
[super viewDidLoad];
_ad = [[ANTSAd alloc] initWithType:kADANTS 
AndSize:ANTSAdSizeHeight250Rectangle  delegate:self];

[_ad loadADWithURI:@"URI"];
[self.view addSubview:[_ad getADView]];
}
```

## Load Facebook AD

ANTS AdX Lite - FACEBOOK
Adding Ad Banners to your ios app

```ruby
Just change AD type from  kADANTS to kADFB .
Example :
_ad = [[ANTSAd alloc] initWithType:kADFB AndSize:ANTSAdSizeHeight250Rectangle  delegate:self];
```


Ad Banner Sizes
The ANTS Audience Network supports three ad sizes to be used in your AdView. The Banner unit's width is flexible with a minimum of 320px, and only the height is defined.
Ad Format

```ruby
Standard Banner    ANTSAdSizeHeight50Rectangle    320x50      
Medium Rectangle   ANTSAdSizeHeight300Rectangle   320x250  This banner is best suited to phones
Large Rectangle    ANTSAdSizeHeight600Rectangle   320x600      
```


Adding Interstitial Ads to your IOS app
Instantiate an FacebookAdView object and make a request to load an ad. Put them in the event that you need to call the Interstitial ads

```ruby
```