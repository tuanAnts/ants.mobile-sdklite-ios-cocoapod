#
#  Be sure to run `pod spec lint ANTSSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|


  s.name         = "ANTSSDKLite"
  s.version      = "0.0.4"
  s.summary      = "A short description of ANTSSDKLite."
  s.description  = "ADSDK - ANTS – Empowering the Digital Media: Ad Exchange, Ad Server, Analytics"

  s.homepage     = "http://ants.vn"


  s.license      = "MIT"
  s.author             = { "tuannv" => "tuannv@ants.vn" }


   s.platform     = :ios
   s.platform     = :ios, "7.0"

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.source       = { :http => "https://www.dropbox.com/s/drilo9344deiyyt/ANTSSDK.framework.zip?dl=1" }
  s.source_files = "AdsWebSDK.framework/**/*.h"
  s.public_header_files =  "ANTSSDK.framework/**/*.h"
  s.vendored_frameworks =  "ANTSSDK.framework"
  s.preserve_paths      =  "ANTSSDK.framework"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

   s.requires_arc = true

   s.dependency  "FBAudienceNetwork" , "~> 4.13.1"

end